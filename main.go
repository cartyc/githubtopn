package main

import (
	"bufio"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

var topN []int

func main() {

	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Println(err)
	}
	defer f.Close()

	r := bufio.NewReader(f)

	for {
		line, err := r.ReadString('\n')
		if err != nil {
			log.Printf("Error: %s\n", err)
			break
		}
		num, err := strconv.Atoi(strings.TrimSpace(line))
		if err != nil {
			log.Printf("Convert Error: %s", err)
		}

		isHighest(num)

	}

	n, _ := strconv.Atoi(os.Args[2])

	// Sort
	sort.Sort(sort.Reverse(sort.IntSlice(topN)))
	log.Println(topN[:n])
}

// Check if new number is higher than was is in list
func isHighest(number int) {

	topN = append(topN, number)

}
